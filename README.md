# min-form-app
Minimalistic form with validation

open command line
1) clone project https://github.com/BrainDrain9/min-form-app.git min-app
// go to min-app folder
$ cd min-app

2) to run project you should have installed Node.js >= 4.0 (you can download https://nodejs.org/en/download/)
// update npm
$ npm install npm -g
// to install dependencies from package.json run
$ npm i

3) you should install bower dependencies. if you don't have bower install it
$ npm install bower -g
// to install bower dependecies from bower.json run
$ bower i

4) to preprocess scss and saas files to css you sould have installed ruby(you can download http://rubyinstaller.org/downloads/)
// if you don't have saas download
$ gem install sass

5) If you have previously installed a version of gulp globally, please install it
$ npm install gulp -g

6) environment is ready! run
$ gulp
// application is opened on localhost:3000

