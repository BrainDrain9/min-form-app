module.exports = function($scope, $http, $state, LocalData, UserData) {
	$scope.states = [];
	$scope.countries = [];
	$scope.user = {};

	// get country list
	LocalData.countries().then(function(response) {
		$scope.countries = response.data;

	});

	// get states list of USA
	LocalData.states().then(function(response) {
		$scope.states = response.data;

	});

	
	$scope.register = function(user) {
		console.log(user, 'user');
		UserData.setData(user);
		// on relad we come back to "form" state to prevent page error on reload of "user" state
		$state.transitionTo ('user', {}, { location: false, inherit: true, relative: $state.$current, notify: true })
	}

}