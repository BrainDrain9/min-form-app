module.exports = function($http) {
	var obj = {states:[], countries:[]};

    obj.states = function() {
    	return $http.get('js/json/states.json').success(function(response) {
			return response;
		})   
	}

	obj.countries = function() {
    	return $http.get('js/json/countries.json').success(function(response) {
			return response;
		})   
	}

    return obj;   
}