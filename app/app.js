require('angular')
require('angular-animate')
require('angular-ui-router')


var LocalData = require('./js/factories/LocalData')
var UserData = require('./js/services/UserData')
var FormController = require('./js/controllers/FormController')

var app = angular.module('app', ['ui.router', 'ngAnimate'])
.config(function ($stateProvider, $urlRouterProvider, $locationProvider) {

    $stateProvider
    .state('form', {
      url: "/",
      templateUrl: "./templates/_form.html",
      controller: "FormController",
      data: {
      	bgClass: "outside"
      }
    })
    .state('user', {
      templateUrl: "./templates/_user.html",
      controller: "UserController",
      data: {
      	bgClass: "inside"
      },
      root: 'form',
      controller: function($scope, UserData) {
        $scope.user = UserData.getData();
      }
    });

    $urlRouterProvider.otherwise('/');

    $locationProvider.html5Mode(true);
  })

.run(function($rootScope, $state) {
	$rootScope.$state = $state;
})

app.factory('LocalData', ['$http', LocalData])
app.service('UserData', [UserData])

app.controller('FormController', ['$scope', '$http', '$state', 'LocalData', 'UserData', FormController])
