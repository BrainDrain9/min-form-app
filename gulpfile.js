var gulp = require('gulp'),
connect = require('gulp-connect'),
browserify = require('browserify'),
bower = require('gulp-bower'),
sass = require('gulp-ruby-sass'),
cleanCSS = require('gulp-clean-css'),
htmlmin = require('gulp-htmlmin'),
imagemin = require('gulp-imagemin'),
jsonminify = require('gulp-jsonminify'),
notify = require('gulp-notify'),
source = require('vinyl-source-stream');

// conect to localhost 3000
gulp.task('connect', function () {
	connect.server({
		root: 'public',
		port: 3000
	})
})

//  get giles from bower_components
var bowerDir = './bower_components';

gulp.task('bower', function() {
    return bower()
        .pipe(gulp.dest(bowerDir))
});

// add fonts
gulp.task('fonts', function() {
    return gulp.src([ './app/fonts/**/*.ttf'])
            .pipe(gulp.dest('./public/fonts/'));
});

// add and minify json
gulp.task('json', function() {
    return gulp.src([ './app/js/json/*.json'])
            .pipe(jsonminify())
            .pipe(gulp.dest('./public/js/json/'));
});

// add and minify img
gulp.task('img', function() {
    return gulp.src([ './app/img/*.*'])
            .pipe(imagemin())
            .pipe(gulp.dest('./public/img/'));
});

// turn scss into css and than minify
gulp.task('css', function() {
        return sass('./app/**/*.scss', {
        style: 'compressed',
        loadPath: ['.app/css', './bower_components/bootstrap-sass/assets/stylesheets']
        })
        .pipe(cleanCSS({debug: true}, function(details) {
            console.log(details.name + ': ' + details.stats.originalSize);
            console.log(details.name + ': ' + details.stats.minifiedSize);
        }))
        .pipe(gulp.dest('./public/'));
});

// add and minify html
gulp.task('html', function() {
    return gulp.src('./app/**/*.html')
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest('./public/'))
});

// js
gulp.task('browserify', function() {
	// Grabs the app.js file
    return browserify('./app/app.js')
    	// bundles it and creates a file called main.js
        .bundle()
        .pipe(source('main.js'))
        // saves it to the public/js/ directory
        .pipe(gulp.dest('./public/js/'));
})


gulp.task('watch', function() {
	gulp.watch('app/**/*.js', ['browserify'])
	gulp.watch('app/**/*.html', ['html'])
	gulp.watch('app/**/*.scss', ['css'])
})

gulp.task('build', ['html', 'css', 'browserify', 'fonts', 'json', 'img'])


gulp.task('default', ['bower', 'build', 'connect', 'watch'])